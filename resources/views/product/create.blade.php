<x-app-layout>
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800">
            {{ __('Add Product') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="mx-auto max-w-7xl sm:px-6 lg:px-8">
            <div class="overflow-hidden bg-white shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ route('products.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-6">
                            <label class="block">
                                <span class="text-gray-700">Title</span>
                                <input type="text" name="title"
                                    class="block w-full @error('title') border-red-500 @enderror mt-1 rounded-md"
                                    placeholder="" value="{{old('title')}}" />
                            </label>
                            @error('title')
                            <div class="text-sm text-red-600">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="mb-6">
                            <label class="block">
                                <span class="text-gray-700">Upc</span>
                                <input type="text" name="upc"
                                    class="block w-full @error('upc') border-red-500 @enderror mt-1 rounded-md"
                                    placeholder="" value="{{old('upc')}}" />
                            </label>
                            @error('upc')
                            <div class="text-sm text-red-600">{{ $message }}</div>
                            @enderror
                        </div>
                     
                        <div class="mb-6">
                            <label class="block">
                                <span class="text-gray-700">Price</span>
                                <input type="text" name="price"
                                    class="block w-full @error('price') border-red-500 @enderror mt-1 rounded-md"
                                    placeholder="" value="{{old('price')}}" />
                            </label>
                            @error('price')
                            <div class="text-sm text-red-600">{{ $message }}</div>
                            @enderror
                        </div>
                     

                        <div class="mb-6">
                            <label class="block">
                                <span class="text-gray-700">Choose File</span>
                                <input type="file" name="image"
                        class="block w-full text-sm text-gray-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-blue-50 file:text-blue-700 hover:file:bg-blue-100" />
                            </label>
                            @error('image')
                            <div class="text-sm text-red-600">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="mb-6 ">
                            <label class="block">
                                <span class="text-gray-700">Status</span>
                                <select name="status" class="block w-full mt-1 rounded-md">
                                    <option value="1">Active</option>
                                    <option value="0">InActive</option>
                                </select>
                            </label>
                            @error('status')
                            <div class="text-sm text-red-600">{{ $message }}</div>
                            @enderror
                        </div>

                        <button type="submit"
                            class=" bg-blue-600 text-white  rounded text-sm px-5 py-2.5">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>