<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()->paginate(10);

        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255',
        ]);

        if($request->file('image'))
        {
            $image_path = $request->file('image')->store('image', 'public');
        }else{
            $image_path = '';
        }
        

        Product::create([
            'title' => $request->title,
            'image' => $image_path,
            'upc' => $request->upc,
            'price' => $request->price,
            'status' => $request->status,
        ]);

        return redirect()->route('products.index')->with('status', 'Product Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $Product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $Product)
    {
        return view('product.show', compact('Product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $Product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $Product)
    {

        return view('product.edit', compact('Product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $Product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $Product)
    {
        $request->validate([
            'title' => 'required|string|max:255',
        ]);
        if($request->file('image'))
        {
            $image_path = $request->file('image')->store('image', 'public');
        }
        $Product->title = $request->title;
        if(isset($image_path))
        {
            $Product->image = $image_path;    
        }
        $Product->price = $request->price;
        $Product->status = $request->status;
        $Product->upc = $request->upc;
        $Product->save();

        return redirect()->route('products.index')->with('status', 'Product Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $Product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $Product)
    {
        $Product->delete();

        return redirect()->route('products.index')->with('status', 'Product Delete Successfully');
    }
}