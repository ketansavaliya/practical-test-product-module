# Getting started

## Installation
Clone the repository

    git clone git@gitlab.com:ketansavaliya/practical-test-product-module.git

Switch to the repo folder

    cd practical-test-product-module

Install all the dependencies using composer

    composer install

Install all the npm dependencies

    npm install
    npm run build

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Other commands

    php artisan storage:link

Start the local development server

    php artisan serve